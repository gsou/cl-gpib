(in-package :gpib)

;;; Hardware interface

(defvar *current-gpib*)
(defvar *stream-gpib*)

(defun gpib-open (name)
  "Opens and return the GPIB port connected to serial port NAME"
  (setf *current-gpib* (open-serial name))
  (setf *stream-gpib* (make-serial-stream *current-gpib*))
  *current-gpib*)

(defun gpib-close (&optional (serial *current-gpib*))
  "Closes the given GPIB port"
  (when (eq serial *current-gpib*)
    (setf *stream-gpib* nil)
    (setf *current-gpib* nil))
  (close-serial serial))

;; Missing serial-posix functions on unix:
;; (defconstant FIONREAD #x541B)
;; (defun input-available (s)
;;   (with-slots (fd) s
;;     (with-foreign-object (bytes :int)
;;       (ioctl fd FIONREAD bytes)
;;       (mem-ref bytes :int))))
;; (defmethod %input-available-p ((s posix-serial)) (> (input-available s) 0))

;;; Interfacing wrapper function:
(defmacro gpib-format-if (serial fmt &rest args)
  `(write-serial-string (format nil ,fmt ,@args) ,serial))
(defmacro gpib-format(fmt &rest args)
  `(gpib-format-if *current-gpib* ,fmt ,@args))

(defun gpib-flush (&optional (serial *current-gpib*))
  (loop while (serial-input-available-p serial) do (read-serial-byte serial)))

(defun gpib-readline-if (serial &key (format 'string))
  (let ((list
          (if (serial-input-available-p serial)
              (loop
                for code = (read-serial-byte serial)
                while (and (/= code 10) (/= code 0))
                when (/= code 13)
                  collect
                  (progn
                    (when (= code 27) (setq code (read-serial-byte serial)))
                    (ecase format
                      (string (code-char code))
                      (vector code)))))))
    (coerce list format)))
(defmacro gpib-readline (&key (format ''string))
  `(gpib-readline-if *current-gpib* :format ,format))

(defun gpib-wait (&optional (serial *current-gpib*))
  (loop until (serial-input-available-p serial)))

(defmacro gpib-cmd (fmt &rest args)
  "Equivalent to (gpib-format FMT . ARGS) (gpib-wait) (gpib-readline)"
  `(progn
     (gpib-format ,(concatenate 'string fmt " ~%") ,@args)
     (gpib-wait)
     (gpib-readline)))


;;; Commands
;;;


(defmacro %flag-cmd (fmt)
  `(if flag-p
      (let ((new-flag (if (or (null flag) (eql 0 flag)) 0 1)))
        (gpib-format ,(concatenate 'string "++" fmt " ~A") new-flag))
      (gpib-cmd ,(concatenate 'string "++" fmt))))

(defun cmd-addr (&optional pad sad)
  (cond
    ((and pad sad) (gpib-format "++addr ~A ~A~%" pad sad))
    (pad (gpib-format "++addr ~A~%" pad))
    (t (let ((i (parse-integer (gpib-cmd "++addr")))) i))))

(defun cmd-auto (&optional (flag nil flag-p))
  (%flag-cmd "auto"))

(defun cmd-clr ()
  (gpib-format "++clr ~%"))

(defun cmd-eoi (&optional (flag nil flag-p))
  (%flag-cmd "eoi"))

(defun cmd-eos (&optional flag)
  (if flag
    (gpib-format "++eos ~D~%" flag)
    (gpib-cmd "++eos")))

(defun cmd-eot (&optional (char nil char-p))
  (if char-p
      (progn
        (gpib-format "++eot_enable ~A~%" (if char 1 0))
        (when char
          (etypecase char
            (character (gpib-format "++eot_char ~A~%" (char-code char)))
            (integer (gpib-format "++eot_char ~A~%" char)))))
      (values (code-char (parse-integer (gpib-cmd "++eot_char")))
              (= 1 (parse-integer (gpib-cmd "++eot_enable"))))))

(defun cmd-ifc ()
  (gpib-format "++ifc ~%"))
(defun cmd-llo ()
  (gpib-format "++llo ~%"))
(defun cmd-llc ()
  (gpib-format "++llc ~%"))

(defun cmd-lon (&optional (flag nil flag-p))
  (%flag-cmd "lon"))

(defun cmd-mode (&optional (flag nil flag-p))
  (%flag-cmd "mode"))

(defun cmd-read-tmo-ms (&optional (ms nil ms-p))
  (if ms-p
      (etypecase ms
          ((integer 1 3000) (gpib-format "++read_tmo_ms ~A~%" ms)))
      (gpib-cmd "++read_tmo_ms")))

(defun cmd-read (&optional eoi)
  (if eoi
      (gpib-format "++read eoi ~%")
      (gpib-format "++read ~%"))
  (gpib-wait))

(defun cmd-rst ()
  (gpib-format "++rst ~%"))

(defun cmd-spoll-async ()
  (gpib-format "++spoll ~%")
  (gpib-wait))

(defun cmd-spoll (&optional pad sad)
  (cond
    ((and pad sad) (etypecase pad
                     ((integer 0 30)
                      (etypecase sad
                        ((integer 96 126)
                         (gpib-format "++spoll ~A ~A~%" pad sad))))))
    (pad
     (etypecase pad ((integer 0 30) (gpib-format "++spoll ~A~%" pad))))
    (t (gpib-format "++spoll ~%")))
  (gpib-wait)
  (gpib-readline))

(defun cmd-srq ()
  (gpib-cmd "++srq"))

(defun cmd-savecfg ()
  (gpib-format "++savecfg ~%"))

(defun cmd-status (&optional status)
  (if status
      (etypecase status
        ((unsigned-byte 8) (gpib-format "++status ~A~%" status)))
      (gpib-cmd "++status")))


(defun cmd-ver ()
  "This command returns the version string of the Fenrir GPIB-USBCDC controller."
  (gpib-cmd "++ver"))

(defun cmd-wait ()
  (loop for srq = (cmd-srq)
        do
           (cond
             ((string= "1" srq) )
             ((string= "0" srq) (return))
             (t (error "Invalid response to srq: ~A" srq)))))
