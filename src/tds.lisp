(in-package :tds)
;;;; Commands and bindings for the TDS Family of Digitizing Oscilloscopes
;;; If any binding you require is missing, refer to the 070-8909-07 Programmer Manual
;;; Commands can be run by name using the generic GPIB-CMD function, but the CMD
;;; and QUERY functions are provided to more easily interact with the TDS specifically.

(defun tds-setup (addr)
  "Set the required parameters to communicate with a TDSxxx scope at address ADDR."
  (cmd-mode 1)
  (cmd-addr addr)
  (cmd-auto 0)
  (cmd-eos 0)
  (cmd-eot nil))

(defun ret ()
  "Get return of a command."
  (loop while (string= (cmd-spoll) "0"))
  (unless (string= (cmd-spoll) "0")
    (cmd-read)
    ;; (sleep 1)
    ;; (cmd-spoll-async)
    ;; (sleep 1)
    (let ((str (gpib-readline)))
      (loop for x = (gpib-readline)
            until (or (string= x "0") (string= x ""))
            do (setq str (concatenate 'string str '(#\NewLine) x)))
      str)))

(defmacro cmd (fmt &rest args)
  `(progn
     (sleep 0.01)
     (gpib-readline)
     (gpib-format ,(concatenate 'string fmt " ~%") ,@args)
     (gpib:cmd-wait)))

(defmacro query (fmt &rest args)
  `(progn (cmd ,fmt ,@args) (ret)))


(defmacro defcmd (varname cmdname)
  "Define a simple command taking no arguments"
  `(defun ,varname () (cmd ,cmdname)))

(defmacro defq (varname cmdname &optional (typ 'string))
  "Define a simple query"
  `(defun ,(intern (concatenate 'string (symbol-name varname) "?")) ()
     (tds-parse ',typ (query ,(concatenate 'string cmdname "?")))))

(defmacro defcmd-std (varname cmdname rawtype &optional (acttype rawtype) )
  "Define a standard command setting and reading a register"
  `(progn
     (defun ,varname (arg)
       (declare (type ,acttype arg))
       (cmd ,(concatenate 'string cmdname " ~A") (tds-format ',rawtype arg)))
     (defun ,(intern (concatenate 'string (symbol-name varname) "?")) ()
       (tds-parse ',rawtype (query ,(concatenate 'string cmdname "?"))))))


;;; TDS Object types and conversion

(deftype NR1 () 'integer)
(deftype NR2 () 'float)
(deftype NR3 () 'float)
(deftype ON-OFF-NR1 ()
  `(or
    (eql :on)
    (eql :off)
    nr1))
(deftype INFINITE-NR1 ()
  `(or
    (eql :INFI)
    (eql :INFINITE)
    nr1))

(deftype CURVE ()
  `(member :REF1 :REF2 :REF3 :REF4 :CH1 :CH2 :CH3 :CH4 :MATH1 :MATH2 :MATH3))

(defparameter *measurement-types* (list :AMP :AMPLITUDE :ARE :AREA :BUR :BURST :CAR :CAREA :CME :CMEAN :CRM :CRMS
           :DEL :DELAY :FALL :FREQ :FREQUENCY :HIGH :LOW :MAX :MAXIMUM :MEAN :MINI :MINIMUM
           :NDU :NDUTY :NOV :NOVERSHOOT :NWI :NWIDTH :PDU :PDUTY :PERIOD :PHASE :PK2 :PK2PK
           :POV :POVERSHOOT :PWI :PWIDTH :RIS :RISE :RMS))
(deftype meas-type () `(member ,@*measurement-types*))

(defun tds-parse (typ str)
  (match typ
    ('NR1 (parse-integer str))
    ('KEYWORD (intern str 'keyword))
    ('NR3 (read-from-string str))
    ('STRING str)
    ('QSTRING
     ;; TODO Escape double quotes
     (read-from-string str))
    ((list 'LIST internal)
     (mapcar (lambda (x) (tds-parse internal x)) (uiop:split-string str :separator '(#\,))))
    (t (error "Don't know how to parse a TDS resonse of type ~A" typ))))

(defun tds-format-string (typ)
  (ecase typ
            (nr1 "~A")
            (nr3 "~,,,,,,'EE")
            (on-off-nr1 "~A")
            (string "~A")
            ;; TODO escape double quotes
            (qstring "~S")
            (keyword "~A")))

(defun tds-format (typ obj)
  (format nil
          (tds-format-string typ)
          obj))


;;; Acquisition Commands
;;; Acquisition commands affect waveform acquisition. These commands control
;;; mode, averaging, enveloping, and single-waveform acquisition.
#|
| Header            | Description                        |
|-------------------+------------------------------------|
| ACQuire?          | Return acquisition parameters      |
| ACQuire:AUTOSAve  | Save waveforms to reference memory |
| ACQuire:MODe      | Acquisition mode                   |
| ACQuire:NUMACq?   | Return # of acquisitions obtained  |
| ACQuire:NUMAVg    | Number of acquisitions for average |
| ACQuire:NUMEnv    | Number of acquisitions for enveloe |
| ACQuire:REPEt     | Repetitive acquisition mode        |
| ACQuire:STATE     | Start or stop aquisition system    |
| ACQuire:STOPAfter | Acquisition control                |
# |#

(defq acquire "ACQ")
(defcmd-std acquire-autosave "ACQ:AUTOSAVE" nr1 on-off-nr1)
(defcmd-std acquire-mode "ACQ:MOD" keyword (member :SAM :SAMLPE :PEAK :PEAKDETECT :HIR :HIRES :AVE :AVERAGE :ENV :ENVELOPE))
(defq acquire-numacq "ACQ:NUMAC" nr1)
(defcmd-std acquire-numavg "ACQ:NUMAV" nr1)
(defcmd-std acquire-numenv "ACQ:NUMENV" nr1 infinite-nr1)
(defcmd-std acquire-repet "ACQ:REPET" nr1 on-off-nr1)
(defcmd-std acquire-state "ACQ:STATE" nr1 (or (member :OFF :ON :RUN :STOP) nr1))
(defcmd-std acquire-stopafter "ACQ:STOPAFTER" keyword (member :RUNST :RUNSTOP :SEQ :SEQUENCE :LIM :LIMIT))


;;; TODO
;;; Application Menu Commands
;;; Application menu commands let you define special-purpose menus. You can
;;; define labels for the main and side menus as well as a side menu title. You can
;;; display an Application menu by either pressing the front-panel APPLICATION
;;; button or sending the APPMenu ACTivate command. Table 2–6 lists these
;;; commands.
;;; When the digitizing oscilloscope displays an Application menu and a user
;;; presses a front-panel button, the oscilloscope generates an event that tells the
;;; controller which button the user pressed. You can also set up the event reporting
;;; system so that it generates a Service Request when a user presses a menu button.
#|
| Header                  | Description                                        |
|-------------------------+----------------------------------------------------+
| APPMenu                 | Display the application menu                       |
| APPMenu:LABel           | Return or remove all aplication menu button labels |
| APPMenu:LABel:BOTTOM<x> | Label for a bottom menu button                     |
| APPMenu:LABel:BOTTOM<x> | Label for a bottom menu button                     |
| APPMenu:LABel:RIGHT<x>  | Label for a right menu button                      |
| APPMenu:TITLe           | Create a title for the app menu                    |

|#


;;; Cursor Commands
;;; Cursor commands provide control over cursor display and readout
#|
| Header                        | Description                                                  |
|-------------------------------+--------------------------------------------------------------|
| CURSor?                       | Return cursor settings                                       |
| CURSor:FUNCtion               | Cursors on or off; select cursor tye                         |
| CURSor:HBArs?                 | Return H bar settings                                        |
| CURSor:HBArs:DELTa?           | Return delta between H bars                                  |
| CURSor:HBArs:POSITION<x>      | Position a horizontal cursor                                 |
| CURSor:HBArs:POSITION<x>Pcnt  | Position a horizontal cursor in units of %                   |
| CURSor:HBArs:SELect           | Set which cursor the knob controls                           |
| CURSor:HBArs:UNIts            | Set H bar units                                              |
| CURSor:MODe                   | Set cusor tracking mode                                      |
| CURSor:PAIred                 | Position paired cursor                                       |
| CURSor:PAIred:HDELTA?         | Return horizontal distance between 1st and 2nd aired cursors |
| CURSor:PAIred:HPOS1?          | Return H position of 1st paired cursor                       |
| CURSor:PAIred:HPOS2?          | Return H position of 2nd paired cursor                       |
| CURSor:PAIred:POSITION<x>Pcnt | Position H paired cursor in units of % of rec len            |
| CURSor:PAIred:POSITION<x>     | Set or return vbar position of the 1st or 2nd paired cursor  |
| CURSor:PAIred:SELect          | Select active paired cursor                                  |
| CURSor:PAIred:UNIts           | Set paired cursor units                                      |
| CURSor:PAIred:VDELTA?         | Return vertical distance between 1st and 2nd paired cursors  |
| CURSor:VBArs                  | Position vertical bar cursors                                |
| CURSor:VBArs:DELTa?           | Return horizontal distance between cursors                   |
| CURSor:VBArs:POSITION<x>      | Position a vertical cursor                                   |
| CURSor:VBArs:POSITION<x>Pcnt  | Position a vertical cursor   in % of record len              |
| CURSor:VBArs:SELect           | Set which cursor the knob controls                           |
| CURSor:VBArs:UNIts            | Set vertical cursors to seconds, frequency, or to lines      |
| CURSor:VBArs:UNITSTring       | Return unit string for the vectical bar cursor                                                             |
# |#

(defq cursor "CURS")
(defcmd-std cursor-function "CURS:FUNC" keyword (member :HBA :HBARS :OFF :VBA :VBARS :PAI :PAIRED))
(defq cursor-hbars "CURS:HBA")
(defq cursor-hbars-delta "CURS:HBA:DELT" nr3)
(defcmd-std cursor-hbars-position1 "CURS:HBA:POSITION1" nr3 real)
(defcmd-std cursor-hbars-position2 "CURS:HBA:POSITION2" nr3 real)
(defcmd-std cursor-hbars-position1pcnt "CURS:HBA:POSITION1PCNT" nr3 real)
(defcmd-std cursor-hbars-position2pcnt "CURS:HBA:POSITION2PCNT" nr3 real)
(defcmd-std cursor-hbars-select "CURS:HBA:SEL" keyword (member :CURSOR1 :CURSOR2))
(defcmd-std cursor-hbars-units "CURS:HBA:UNI" keyword (member :BAS :BASE :IRE))
;; Track ties tho two cursor together as you move the general purpose knob
(defcmd-std cursor-mode "CURS:MOD" keyword (member :TRACK :TRAC :IND :INDEPENDANT))
(defcmd-std cursor-paired "CURS:PAIRED" string (member :SNA :SNAP))
(defq cursor-paired-hdelta "CURS:PAI:HDELTA" nr3)
(defq cursor-paired-hpos1 "CURS:PAI:HPOS1" nr3)
(defq cursor-paired-hpos2 "CURS:PAI:HPOS2" nr3)
(defcmd-std cursor-paired-position1 "CURS:PAI:POSITION1" nr3 real)
(defcmd-std cursor-paired-position2 "CURS:PAI:POSITION2" nr3 real)
(defcmd-std cursor-paired-position1pcnt "CURS:PAI:POSITION1PCNT" nr3 real)
(defcmd-std cursor-paired-position2pcnt "CURS:PAI:POSITION2PCNT" nr3 real)
(defcmd-std cursor-paired-select "CURS:PAI:SEL" keyword (member :CURSOR1 :CURSOR2))
(defcmd-std cursor-paired-units "CURS:PAI:UNI" keyword (member :BAS :BASE :IRE))
(defq cursor-paired-vdelta "CURS:PAI:VDELTA" nr3)

(defcmd-std cursor-vbars "CURS:VBARS" string (member :SNA :SNAP))
(defq cursor-vbars-delta "CURS:VBA:DELT" nr3)
(defcmd-std cursor-vbars-position1 "CURS:VBA:POSITION1" nr3 real)
(defcmd-std cursor-vbars-position2 "CURS:VBA:POSITION2" nr3 real)
(defcmd-std cursor-vbars-position1pcnt "CURS:VBA:POSITION1PCNT" nr3 real)
(defcmd-std cursor-vbars-position2pcnt "CURS:VBA:POSITION2PCNT" nr3 real)
(defcmd-std cursor-vbars-select "CURS:VBA:SEL" keyword (member :CURSOR1 :CURSOR2))
(defcmd-std cursor-vbars-units "CURS:VBA:UNI" keyword (member :SECO :SECONDS
                                                              :HER :HERTZ
                                                              :LINE :BAS :BASE
                                                              :INV :INVERT))
(defq cursor-vbars-unitstring "CURS:VBA:UNITST" keyword)



;;; Display Commands
;;; Display commands let you change the graticule style, change the displayed
;;; intesities, display messages, and clear the menu.
#|
| Header                | Description                                   |
|-----------------------+-----------------------------------------------|
| CLEARMenu             | Clear menus from display                      |
| DISplay?              | Return display settings                       |
| DISplay:CLOCk         | Control the display of the date/time stamp    |
| DISplay:COLOr:...     | Color                                         |
| DISplay:FILTer        | Displayed data interolation                   |
| DISplay:FORMat        | YT or XY display                              |
| DISplay:GRAticule     | Graticule style                               |
| DISplay:INStavu:...   | InstaVu                                       |
| DISplay:INTENSITy:... | Brightness                                    |
| DISplay:MODe          | Normal or InstaVu dislay                      |
| DISplay:PERSistence   | Variable persistence decay time               |
| DISplay:STYle         | Waveform dots, vector, ...                    |
| DISplay:TRIGBar       | Display of the trigger bars                   |
| DISplay:TRIGT         | Control the display of the trigger indicator  |
| MESSage               | Remove text from message window               |
| MESSage:BOX           | Set size and location of message window       |
| MESSage:SHOw          | Remove and display text in the message window |
| MESSage:STATE         | Control display of message window             |
                                        ;
|#

;;; Raw commands mapping
(defcmd clearmenu "CLEARM")
(defq display "DIS?")

(defcmd-std display-clock "DIS:CLOC" nr1 on-off-nr1)

;; TODO Color

(defcmd-std display-filter "DIS:FILT" keyword (member :LINEA :LINEAR :SINX))
(defcmd-std display-format "DIS:FORM" keyword (member :XY :YT))
(defcmd-std display-graticule "DIS:GRA" keyword (member :CROSSH :CROSSHAIR :FRA :FRAME :FUL :FULL :GRI :GRID :NTS :NTSC :PAL))

;; TODO Instavu
;; TODO Intensity

(defcmd-std display-mode "DIS:MODE" keyword (member :INS :INSTAVU :NORM :NORMAL))
(defcmd-std display-persistence "DIS:PERS" float real)
(defcmd-std display-style "DIS:STY" keyword (member :DOT :DOTS :INFP :INFPERSIST :INTENSIF :INTENSIFIED :VAR :VARPERSIST :VECTORS :VEC))
(defcmd-std display-trigbar "DIS:TRIGB" keyword (member :OFF :SHOR :SHORT :LONG))
(defcmd-std display-trigt "DIS:TRIGT" nr1 on-off-nr1)

(defcmd message-clear "MESS CLE")
(defq message "MESS")


(defun message-box (x1 y1 x2 y2)
  (declare (type (integer 0 (640)) x1 x2)
           (type (integer 0 (480)) y1 y2))
  (cmd "MESS:BOX ~A,~A,~A,~A" x1 y1 x2 y2))
(defq message-box "MESS:BOX" (list nr1))

(defcmd-std message-show "MESS:SHOW" qstring string)
(defcmd-std message-state "MESS:STATE" nr1 on-off-nr1)


;;; TODO
;;; File system commands
;;;



;;; Hardcopy Commands
#|
| Header            | Description                                |
|-------------------+--------------------------------------------|
| HARDCopy          | Start or terminate hardcopy                |
| HARDCopy:FILEName | Select file to send hardcopy data to       |
| HARDCopy:FORMat   | Hardcopy output format                     |
| HARDCopy:LAYout   | Hardcopy orientation                       |
| HARDCopy:PALEtte  | Select palette to use when making hardcopy |
| HARDCopy:PORT     | Hardcopy port for output                   |
# |#

(defcmd-std hardcopy "HARDC" string (member :ABO :ABORT :CLEARS :CLEARSPOOL :STAR :START))
(defcmd-std hardcopy-filename "HARDC:FILEN" qstring string)
(defcmd-std hardcopy-format "HARDC:FORM" keyword (member :BMP :BMCOLOR :DESKJ :DESKJET :DPU411 :DPU412 :EPSCOLI :EPSCOLIMG :EPSC :EPSCOLOR :EPSI :EPSIMAGE :EPSM :EPSMONO :EPSO :EPSON :HPG :HPGL :INTERL :INTERLEAF :LASERJ :LASERJET :PCX :PCXCOLOR :RLE :THI :THINKJET :TIF :TIFF))
(defcmd-std hardcopy-layout "HARDC:LAY" keyword (member :LAN :LANDSCAPE :PORTR :PORTRAIT))
(defcmd-std hardcopy-palette "HARDC:PALE" keyword (member :CURR :CURRENT :HARDC :HARDCOPY))
(defcmd-std hardcopy-port "HARDC:PORT" keyword (member :CEN :CENTRONICS :FILE :GPI :GPIB :RS232))


;;; Horizontal Commands
#|
| Header                      | Description                         |
|-----------------------------+-------------------------------------|
| HORizontal?                 | Return horizontal settings          |
| HORizontal:CLOck            | Enable internal or external clocks  |
| HORizontal:CLOck:MAXRate    | Set maximum external clock rate     |
| HORizontal:DELay?           | Return delay time base settings     |
| HORizontal:DELay:*          |                                     |
| HORizontal:FASTframe:*      |                                     |
| HORizontal:MAIn?            | Return main time per division       |
| HORizontal:MAIn:SCAle       | Main time base per division         |
| HORizontal:MAIn:SECdiv      | Same as HORizontal:MAIn:SCAle       |
| HORizontal:MODe             | Turn delay time base on or off      |
| HORizontal:POSition         | Portion of waveform to display      |
| HORizontal:RECOrdlength     | Number of points in waveform record |
| HORizontal:ROLL             | Set roll mode to auto or off        |
| HORizontal:SCAle            | Same as HOR:MAI:SCA                 |
| HORizontal:SECdiv           | Same as HOR:MAI:SCA                 |
| HORizontal:TRIGger?         | Return trigger position             |
| HORizontal:TRIGger:POSition | Main time base trigger position     |
# |#


(defq horizontal "HORIZONTAL")
(defcmd-std horizontal-clock "HOR:CLO" keyword (member :INT :INTERNAL :EXT :EXTERNAL))
(defcmd-std horizontal-clock-maxrate "HOR:CLO:MAXR" nr3)
(defq horizontal-delay "HOR:DEL")
(defcmd-std horizontal-delay-mode "HOR:DEL:MOD" keyword (member :RUNSA :RUNSAFTER :TRIGA :TRIGAFTER))
(defcmd-std horizontal-delay-scale "HOR:DEL:SCA" nr3)
(defcmd-std horizontal-delay-secdiv "HOR:DEL:SEC" nr3)
(defcmd-std horizontal-delay-time "HOR:DEL:TIM" nr3)
(defcmd-std horizontal-delay-time-runsafter "HOR:DEL:TIM:RUNSA" nr3)
(defcmd-std horizontal-delay-time-trigafter "HOR:DEL:TIM:TRIGA" nr3)
(defcmd-std horizontal-fastframe-count "HOR:FAST:COUNT" nr1)
(defcmd-std horizontal-fastframe-length "HOR:FAST:LEN" nr1)
(defcmd-std horizontal-fastframe-position "HOR:FAST:POS" nr1)
(defcmd-std horizontal-fastframe-state "HOR:FAST:STATE" nr1 on-off-nr1)
(defcmd-std horizontal-fittoscreen "HOR:FIT" nr1 on-off-nr1)
(defq horizontal-main "HOR:MAIN")
(defcmd-std horizontal-main-scale "HOR:MAI:SCA" nr3)
(defcmd-std horizontal-main-secdiv "HOR:MAI:SEC" nr3)
(defcmd-std horizontal-mode "HOR:MOD" keyword (member :DELAYE :DELAYED :INTENSIF :INTENSIFIED :MAI :MAIN))
(defcmd-std horizontal-position "HOR:POS" nr3)
(defcmd-std horizontal-recordlength "HOR:RECO" nr1)
(defcmd-std horizontal-scale "HOR:SCA" nr3)
(defcmd-std horizontal-secdiv "HOR:SEC" nr3)
(defq horizontal-trigger "HOR:TRIG")
(defcmd-std horizontal-trigger-position "HOR:TRIG:POS" nr1)


;;; TODO
;;; Limit Test Commands
;;; The Limit Test commands let you automatically compare each incoming
;;; waveform against a template waveform. You set an envelope of limits around a
;;; waveform and let the digitizing oscilloscope find the waveforms that fall outside
;;; those limits. When it finds such a waveform, it can generate a hardcopy, ring a
;;; bell, stop and wait for your input, or any combination of these actions. Table


;;; TODO
;;; Measurement Commands
#|
| Header                    | Description                                                   |
|---------------------------+---------------------------------------------------------------|
| MEASUrement?              | Return all measurement parameters                             |
| MEASUrement:CLEARSNapshot | Take down measurement snapshot                                |
| MEASUrement:GATING        | Set or query measurement gating                               |
| MEASUrement:*             | Return immediate measurement parameters                       |
| MEASUrement:*:DELay*      |                                                               |
| MEASUrement:*:SOURCE      | Channel to take measurement from                              |
| MEASUrement:*:SOURCE2     | Second channet to take measurement from (delay or to channel) |
| MEASUrement:*:TYPe        | The measurement to be taken                                   |
| MEASUrement:*:UNIts?      | Measurement units                                             |
| MEASUrement:*:VALue?      | Measurement result                                            |
| MEASUrement:MEASx:STATE   | Turn measurement display on or off                            |
# |#

(query measurement "MEASU?")
(defcmd measurement-clearsnapshot "MEASURE:CLEARSNAPSHOT")
(defcmd-std measurement-gating "MEASU:GAT" nr1 on-off-nr1)

(defmacro def-measurement-commands ((&rest meas))
  `(progn

     (deftype meas () `(member ,,@meas))
     (defparameter *measurements* ',meas)

     ,@(loop for m in meas collect
             `(progn
                ;; MEAS specific commands
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m))))
                   `(defq ,(intern str) ,(substitute #\: #\- str)))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-DELAY")))
                   `(defq ,(intern str) ,(substitute #\: #\- str)))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-DELAY-DIRECTION")))
                   `(defcmd-std ,(intern str) ,(substitute #\: #\- str)
                       keyword
                       (member :BACKW :BACKWARDS :FORW :FORWARDS)))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-DELAY-EDGE1")))
                   `(defcmd-std ,(intern str) ,(substitute #\: #\- str)
                       keyword
                       (member :FALL :RIS :RISE)))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-DELAY-EDGE2")))
                   `(defcmd-std ,(intern str) ,(substitute #\: #\- str)
                       keyword
                       (member :FALL :RIS :RISE)))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-SOURCE")))
                   `(defcmd-std ,(intern str) ,(substitute #\: #\- str) keyword curve))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-SOURCE1")))
                   `(defcmd-std ,(intern str) ,(substitute #\: #\- str) keyword curve))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-SOURCE2")))
                   `(defcmd-std ,(intern str) ,(substitute #\: #\- str) keyword curve))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-TYPE")))
                   `(defcmd-std ,(intern str) ,(substitute #\: #\- str) keyword meas-type))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-UNITS")))
                   `(defq ,(intern str) ,(substitute #\: #\- str) qstring))
                ,(let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-VALUE")))
                   `(defq ,(intern str) ,(substitute #\: #\- str) nr3))
                ,(if (alexandria:starts-with #\M (symbol-name m))
                     (let ((str (concatenate 'string "MEASUREMENT-" (symbol-name m) "-STATE")))
                       `(defcmd-std ,(intern str) ,(substitute #\: #\- str) nr1 on-off-nr1)))))))

(def-measurement-commands (:MEAS1 :MEAS2 :MEAS3 :MEAS4 :IMM :IMMED))

(defun measurement-delay? (m) (declare (type meas m )) (query "MEASUREMENT:~A:DELAY?" m))
(defun measurement-units? (m) (declare (type meas m ))
  (tds-parse 'qstring (query "MEASUREMENT:~A:UNITS?" m)))
(defun measurement-value? (m) (declare (type meas m ))
  (tds-parse 'nr3 (query "MEASUREMENT:~A:VALUE?" m)))

(defun measurement-delay-direction? (m) (declare (type meas m))
  (tds-parse 'keyword (query "MEASUREMENT:~A:DELAY:DIRECTION?" m)))
(defun measurement-delay-direction (m v) (declare (type meas m) (type (member :BACKW :BACKWARDS :FORW :FORWARDS) v)) (cmd "MEASUREMENT:~A:DELAY:DIRECTION ~A" m v))
(defun measurement-delay-edge1? (m) (declare (type meas m))
  (tds-parse 'keyword (query "MEASUREMENT:~A:DELAY:EDGE1?" m)))
(defun measurement-delay-edge1 (m v) (declare (type meas m) (type (member :FALL :RIS :RISE) v)) (cmd "MEASUREMENT:~A:DELAY:EDGE1 ~A" m v))
(defun measurement-delay-edge2? (m) (declare (type meas m))
  (tds-parse 'keyword (query "MEASUREMENT:~A:DELAY:EDGE2?" m)))
(defun measurement-delay-edge2 (m v) (declare (type meas m) (type (member :FALL :RIS :RISE) v)) (cmd "MEASUREMENT:~A:DELAY:EDGE2 ~A" m v))
(defun measurement-source? (m) (declare (type meas m ))
  (tds-parse 'keyword (query "MEASUREMENT:~A:SOURCE?" m)))
(defun measurement-source (m v) (declare (type meas m) (type curve v)) (cmd "MEASUREMENT:~A:SOURCE ~A" m v))
(defun measurement-source1? (m) (declare (type meas m ))
  (tds-parse 'keyword (query "MEASUREMENT:~A:SOURCE1?" m)))
(defun measurement-source1 (m v) (declare (type meas m) (type curve v)) (cmd "MEASUREMENT:~A:SOURCE1 ~A" m v))
(defun measurement-source2? (m) (declare (type meas m ))
  (tds-parse 'keyword (query "MEASUREMENT:~A:SOURCE2?" m)))
(defun measurement-source2 (m v) (declare (type meas m) (type curve v)) (cmd "MEASUREMENT:~A:SOURCE2 ~A" m v))
(defun measurement-type? (m) (declare (type meas m ))
  (tds-parse 'keyword (query "MEASUREMENT:~A:TYPE?" m)))
(defun measurement-type (m v) (declare (type meas m) (type meas-type v)) (cmd "MEASUREMENT:~A:TYPE ~A" m v))
(defun measurement-state? (m) (declare (type meas m ))
  (tds-parse 'keyword (query "MEASUREMENT:~A:STATE?" m)))
(defun measurement-state (m v) (declare (type meas m) (type on-off-nr1 v)) (cmd "MEASUREMENT:~A:STATE ~A" m v))


;;; TODO
;;; Miscellaneous Commands
#|
| Header       | Description                                             | ; ;
|--------------+---------------------------------------------------------| ; ;
| AUTOSet      | Automatic instrument setup                              | ; ;
| BELl         | Audio alert                                             | ; ;
| *DATE        | Set date                                                | ; ;
| *DDT         | Define group execute trigger                            | ; ;
| FACtory      | Reset to factory default                                | ; ;
| HDR / HEADer | Return command header with query                        | ; ;
| *IDN?        | Identification                                          | ; ;
| *LRN?        | Learn device setting                                    | ; ;
| LOCk         | Lock front panel                                        | ; ;
| NEWpass      | Change password for User Protected Data                 | ; ;
| PASSWord     | Access to change User Protected Data                    | ; ;
| REM          | Comment                                                 | ; ;
| SET?         | Same as *LRN?                                           | ; ;
| TEKSecure    | Initialize waveforms and setus                          | ; ;
| *TIMe        | Set time                                                | ; ;
| *TRG         | Perform Group Execute Trigger (GET)                     | ; ;
| *TST?        | Self-test                                               | ; ;
| UNLock       | Unlock front panel                                      | ; ;
| VERBose      | Return full command name or minimum sellings with query | ; ;
# |#




;;; TODO
;;; RS-232 Commands
#|
| Header             | Description              | ; ;
|--------------------+--------------------------| ; ;
| RS232?             | Return RS-232 Parameters | ; ;
| RS232:BAUd         | Set baud rate            | ; ;
| RS232:HARDFlagging | Set hard flagging        | ; ;
| RS232:PARity       | Set arity                | ; ;
| RS232:SOFTFlagging | Set soft flagging        | ; ;
| RS232:STOPBits     | Set # of stop bits       | ; ;
# |#


;;; TODO
;;; Save and Recall Commands
;;; ands
;;; Save and Recall commands allow you to store and retrieve internal waveforms
;;; and settings. When you “save a setup,” you save all the settings of the digitizing
;;; oscilloscope. When you then “recall a setup,” the digitizing oscilloscope restores
;;; itself to the state it was in when you originally saved that setting.


;;; TODO
;;; Status and Error Commands


;;; TODO
;;; Trigger Commands
;;; Trigger commands control all aspects of digitizing oscilloscope triggering.
;;; There are two triggers, main and delayed. Where appropriate, the command set
;;; has parallel constructions for each trigger.
;;; You can set the main or delayed triggers to edge mode. Edge triggering lets you
;;; display a waveform at or near the point where the signal passes through a voltage
;;; level of your choosing.
;;; You can also set TDS 5XXA, 6XXA, and 7XXA main triggers to pulse and logic
;;; modes. Pulse triggering lets the oscilloscope trigger whenever it detects a pulse
;;; of a certain width or height. Logic triggering lets you logically combine the
;;; signals on one or more channels. The digitizing oscilloscope then triggers when


;;; TODO
;;; Vertical Commands
;;; Vertical commands control the display of channels and of main and reference
;;; waveforms.
;;; The SELect:<wfm> command also selects the waveform many commands in
;;; other command groups use.
;;; You may replace VOLts for SCAle in the vertical commands. This provides
;;; program compatibility with earlier models of Tektronix digitizing oscilloscopes.




(defun sel () (tds-parse 'keyword (car (last (uiop:split-string (query "SEL?") :separator '(#\;))))))


;;; Waveform Commands
;; Waveform commands let you transfer waveform data points to and from the
;; digitizing oscilloscope. Waveform data points are a collection of values that
;; define a waveform. One data value usually represents one data point in the
;; waveform record. When working with enveloped waveforms, each data value is
;; either the min or max of a min/max pair. Before you transfer waveform data, you
;; must specify the data format, record length, and waveform locations.
;;
;;; Waveform data Formats
;; Acquired waveform data uses either one or two 8-bit data bytes to represent each
;; data point. The number of bytes used depends on the acquisition mode specified
;; when you acquired the data. Data acquired in SAMple, ENVelope, or PEAKde-
;; tect mode uses one 8-bit byte per waveform data point. Data acquired in HIRes
;; or AVErage mode uses two 8-bit bytes per point. For more information on the
;; acquisition modes see the ACQuire: MODe command on page 2–40.
;; The DATa:WIDth command lets you specify the number of bytes per data point
;; when transferring data to and from the digitizing oscilloscope. If you specify two
;; bytes for data that uses only one, the least significant byte will be filled with
;; zeros. If you specify one byte for data that uses two, the least significant byte
;; will be ignored.
;; The digitizing oscilloscope can transfer waveform data in either ASCII or binary
;; format. You specify the format with the DATa:ENCdg command.
;;
;;; Waveform data record length
;; You can transfer multiple points for each waveform record. You can transfer a
;; portion of the waveform or you can transfer the entire record. The DATa:STARt
;; and DATa:STOP commands let you specify the first and last data points of the
;; waveform record.
;; When transferring data into the digitizing oscilloscope, you must specify the
;; location of the first data point within the waveform record. For example, when
;; you set DATa:STARt to 1, data points will be stored starting with the first point
;; in the record, and when you set DATa:STARt to 500, data will be stored starting
;; at the 500th point in the record. The digitizing oscilloscope will ignore
;; DATa:STOP when reading in data as it will stop reading data when it has no
;; more data to read or when it has reached the specified record length.
;; When transferring data from the digitizing oscilloscope, you must specify the
;; first and last data points in the waveform record. Setting DATa:STARt to 1 and
;; DATa:STOP to the record length will always return the entire waveform. You can
;; also use the vertical bar cursors to delimit the portion of the waveform that you
;; want to transfer. DATa:STARt and DATa:STOP can then be set to the current
;; cursor positions by sending the command DATa SNAp
;;
;;; Waveform Data Locations and Memory AllocatioThe
;; The DATa:SOUrce command specifies the data location when transferring
;; waveforms from the digitizing oscilloscope. You can transfer out multiple
;; waveforms at one time by specifying more than one source.
;; You can transfer in to the digitizing oscilloscope only one waveform at a time.
;; Waveforms sent to the oscilloscope are always stored in one of the four reference
;; memory locations. You can specify the reference memory location with the
;; DATa:DESTination command. You must define the memory size for the
;; specified location before you store the data. The ALLOcate:
;; WAVEFORM:REF<x> command lets you specify the memory size for each
;; reference location
;;
;;; Waveform Preamble
;; Each waveform that you transfer has an associated waveform preamble that
;; contains information such as the horizontal scale, the vertical scale, and other
;; settings in place when the waveform was created.
;;
;;; Scaling Waveform Data
;; Once you transfer the waveform data to the controller, you can convert the data
;; points into voltage values for analysis using information from the waveform
;; preamble.

;;; Transfering Waveform Data from the TDS Family Oscilloscope
;; You can transfer waveforms from the digitizing oscilloscope to an external
;; controller using the following sequence:
;; 1. Select the waveform source(s) using the DATa:SOUrce command. If you
;; want to transfer multiple waveforms, select more than one source.
;; 2. Specify the waveform data format using DATa:ENCdg.
;; 3. Specify the number of bytes per data point using DATa:WIDth.
;; 4. Specify the portion of the waveform that you want to transfer using
;; DATa:STARt and DATa:STOP.
;; 5. Transfer waveform preamble information using WFMPRe? query.
;; 6. Transfer waveform data from the digitizing oscilloscope using the CURVe?
;; query.
;;
;;; Transfering Waveform Data to the TDS Family Oscilloscope
;; You can transfer waveform data to one of the four reference memory locations in
;; the digitizing oscilloscope using the following sequence:
;; 1. Specify waveform reference memory using DATa:DESTination.
;; 2. Specify the memory size for the reference location specified in Step 1 using
;; the ALLOcate:WAVEFORM:REF<x> command.
;; 3. Specify the waveform data format using DATa:ENCdg.
;; 4. Specify the number of bytes per data point using DATa:WIDth.
;; 5. Specify first data point in the waveform record using DATa:STARt.
;; 6. Transfer waveform preamble information using WFMPRe:<wfm>.
;; 7. Transfer waveform data to the digitizing oscilloscope using CURVe.
#|
| Header           | Description                                 | ;
|------------------+---------------------------------------------| ;
| CURVe            | Transfer waveform data                      | ;
| DATa             | Waveform data fromat and location           | ;
| DATa:DESTination | Destination for waveforms sent to the scope | ;
| DATa:ENCdg       | Waveform data encoding method               | ;
| DATa:SOUrce      | Source of CURVe? data                       | ;
| DATa:STARt       | Starting point in waveform transfer         | ;
| DATa:STOP        | Ending point in waveform transfer           | ;
| DATa:TARget      | Same as DATa:DESTination                    | ;
| DATa:WIDth       | Byte width of waveform points               | ;
| WAVFrm?          | Return waveform preamble and data           | ;
| WAVPre?          | Return waveform format data                 | ;
| WFMPre:...       |                                             | ;
# |#

(defcmd-std data "DAT" string (member :INIT :SNA :SNAP))
(defcmd-std data-destination "DAT:DEST" keyword (member :REF1 :REF2 :REF3 :REF4))
(defcmd-std data-encdg "DAT:ENC" keyword (member :ASCI :ASCII :RIB :RIBINARY :RPB :RPBINARY :SRI :SPIBINARY :SRP :SPRBINARY))
(defcmd-std data-source "DAT:SOU" keyword curve)
(defcmd-std data-start "DAT:START" nr1 (integer 1))
(defcmd-std data-stop "DAT:STOP" nr1 (integer 1))
(defcmd-std data-width "DAT:WID" nr1 (integer 1 2))
(defq wavfrm "WAVF")
(defq wfmpre "WFWP")
(defcmd-std wfmpre-bit-nr "WFMP:BIT_N" nr1 (or (eql 8) (eql 16)))
(defcmd-std wfmpre-bn_fmt "WFMP:BN_F" keyword (member :RI :RP))
(defcmd-std wfmpre-byt-nr "WFMP:BYT_N" nr1 (integer 1 2))
(defcmd-std wfmpre-byt-or "WFMP:BYT_O" keyword (member :LSB :MSB))
(defcmd-std wfmpre-encdg "WFMP:ENC" keyword (member :ASC :BIN))
;; TODO PT_FMT


;;; TODO
;;; Zoom Commands



;;; Status and Events
;; DESER selects the enabled bits in the SESR, modify and read with DESE DESE?

(defconstant event-none 0)
(defconstant event-none-pending 1)
(defconstant event-cmd-error 100)
(defconstant event-invalid-character 101)
(defconstant event-syntax-error 102)
(defconstant event-invalid-separator 103)
(defconstant event-get-not-allowed 105)
(defconstant event-invalid-program-data-separator 106)
(defconstant event-parameter-not-allowed 108)
(defconstant event-missing-parameter 109)
(defconstant event-command-header-error 110)
(defconstant event-header-separator-error 111)
(defconstant event-program-mnemonic-too-long 112)
(defconstant event-undefined-header 113)
(defconstant event-query-not-allowed 118)
(defconstant event-numeric-data-error 120)
(defconstant event-invalid-character-in-number 121)
(defconstant event-exponent-too-large 123)
(defconstant event-too-many-digits 124)
(defconstant event-numeric-data-not-allowed 128)
(defconstant event-suffix-error 130)
(defconstant event-invalid-suffix 131)
(defconstant event-suffix-too-long 134)
(defconstant event-suffix-not-alowed 138)
(defconstant event-character-data-error 140)
(defconstant event-invalid-character-data 144)
(defconstant event-character-data-not-allowed 148)
(defconstant event-string-data-error 150)
(defconstant event-invalid-string-data 151)
(defconstant event-string-data-too-long 152)
(defconstant event-string-data-not-allowed 158)
(defconstant event-block-data-error 160)
(defconstant event-invalid-block-data 161)
(defconstant event-block-data-not-allowed 168)
(defconstant event-expression-error 170)
(defconstant event-invalid-expression 171)
(defconstant event-expression-data-not-allowed 178)
(defconstant event-alias-error 180)
(defconstant event-invalid-outside-alias-definition 181)
(defconstant event-invalid-inside-alias-definition 183)
(defconstant event-command-in-alias-requires-more/fewer-parameters 184)
;; TODO Execution error
(defconstant event-device-specific-error 300)
(defconstant event-system-error 310)
(defconstant event-memory-error 311)
(defconstant event-pud-memory-lost 312)
(defconstant event-calibration-memory-lost 313)
(defconstant event-save/recall-memory-lost 314)
(defconstant event-configuration-memory-lost 315)
(defconstant event-queue-overflow 350)
(defconstant event-query-event 400)
(defconstant event-power-on 401)
(defconstant event-operation-complete 402)
(defconstant event-user-request 403)
(defconstant event-power-fail 404)
(defconstant event-request-control 405)
(defconstant event-query-interrupted 410)
(defconstant event-query-unterminated 420)
(defconstant event-query-deadlocked 430)
(defconstant event-query-unterminated-after-indefinite-resonse 440)
(defconstant event-right-menu-button-1 450)
(defconstant event-right-menu-button-2 451)
(defconstant event-right-menu-button-3 452)
(defconstant event-right-menu-button-4 453)
(defconstant event-right-menu-button-5 454)
(defconstant event-bottom-menu-button-1 460)
(defconstant event-bottom-menu-button-2 461)
(defconstant event-bottom-menu-button-3 462)
(defconstant event-bottom-menu-button-4 463)
(defconstant event-bottom-menu-button-5 464)
(defconstant event-bottom-menu-button-6 464)
(defconstant event-bottom-menu-button-7 466)
;; TODO Execution warning
(defconstant event-internal-warning 600)
(defconstant event-bad-thermistor 620)
(defconstant event-50ohm-overload 630)

(defq sesr "*ESR" nr1)
(defun pon? (&optional (sesr (sesr?))) (logbitp 7 sesr))
(defun urq? (&optional (sesr (sesr?))) (logbitp 6 sesr))
(defun cme? (&optional (sesr (sesr?))) (logbitp 5 sesr))
(defun exe? (&optional (sesr (sesr?))) (logbitp 4 sesr))
(defun dde? (&optional (sesr (sesr?))) (logbitp 3 sesr))
(defun qye? (&optional (sesr (sesr?))) (logbitp 2 sesr))
(defun opc? (&optional (sesr (sesr?))) (logbitp 0 sesr))

(defcmd-std eser "*ESE" nr1 (integer 0 255))

(defq event "EVENT" nr1)
(defq evmsg "EVMSG")


;;; Higher level "programs" and command sequences

(defun application-poll (callback)
  "This function continuously poll for an URQ and calls the callback function when a button in the application menu is pressed."
  (unwind-protect
       (progn
         (eser #b01000000)
         (loop for s = (parse-integer (progn (sleep 0.2) (cmd-spoll)))
               when (and (logbitp 5 s) (urq?))
                 do (loop for ev = (event?)
                          while (> ev 1)
                          do (funcall callback ev))))
    (eser 0)))

(defun data-select-all ()
  "Select the entirety of the available data. Equivalent to (data-start 1) (data-stop MAX)"
  (data-start 1)
  (data-stop (horizontal-recordlength?)))

(defun down-curve-to-v (curve &key (width 1))
  "Reads the curve CURVE and convert the values to V."
  (declare (type curve curve)
           (type (integer 1 2) width))
  (data-encdg :ASCII)
  (data-source curve)
  (data-width width)
  (let* ((pos (tds-parse 'nr3 (query "~A:POS?" curve)))
         (factor (tds-parse 'nr3 (query "~A:SCA?" curve)))
         (data (mapcar #'parse-integer (uiop:split-string (query "curve?") :separator '(#\,)))))
    (mapcar (lambda (x) (* factor (- (/ x (if (= 1 width) 25.6 6553.6)) pos))) data)))

(defun down-time-to-s ()
  "Reads the currently selected data-start/data-stop range and return the time position of each points in seconds."
  (let ((start (data-start?))
        (stop (data-stop?))
        (factor (horizontal-scale?)))
    (mapcar
     (lambda (x) (/ (* x factor) 50))
     (loop for i from start to stop collect i))))

(defun down-selection (&key (width 1))
  "Reads the selected region from the selected curve"
  (data :snap)
  (down-curve-to-v (sel) :width width))

(defmacro down-curves ((&rest curves) &key (width 1) (map #'list))
  "Reads the given CURVES. use T as a standin for the currently selected curve."
  `(mapcar ,map
           (down-time-to-s)
           ,@(mapcar
              (lambda (c) `(down-curve-to-v ,(if (eq c t) `(sel) c) :width ,width))
              curves)))

(defmacro format-down-curves (stream fmt (&rest curves) &key (width 1))
  "Format the result of the download to the stream."
  (if stream
      (let ((x (gensym)))
        `(dolist (,x (down-curves ,curves :width ,width)) (format ,stream ,fmt ,x)))
      (let ((x (gensym)))
        `(mapcar (lambda (,x) (format nil ,fmt ,x)) (down-curves ,curves :width ,width)))))

(defun measure (source type)
  "Take the measurement TYPE on channel SOURCE"
  (declare (type curve source) (type meas-type type))
  (measurement-imm-type type)
  (measurement-imm-source source)
  (measurement-imm-value?))

(defun measure-all (source)
  "Take all the available measurements from the source SOURCE. It takes a while and returns a list of (MEAS-TYPE VALUE)"
  (loop for m in *measurement-types*
        collect (list m (measure source m))))
