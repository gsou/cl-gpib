(defpackage :gpib
  (:use :cl :cl-user :cserial-port )
  (:export
   #:gpib-open
   #:gpib-close
   #:gpib-format-if
   #:gpib-format
   #:gpib-readline-if
   #:gpib-readline
   #:gpib-wait
   #:gpib-cmd
   #:cmd-addr
   #:cmd-auto
   #:cmd-clr
   #:cmd-eoi
   #:cmd-eos
   #:cmd-eot
   #:cmd-ifc
   #:cmd-llo
   #:cmd-llc
   #:cmd-lon
   #:cmd-mode
   #:cmd-read-tmo-ms
   #:cmd-read
   #:cmd-rst
   #:cmd-spoll
   #:cmd-srq
   #:cmd-savecfg
   #:cmd-status
   #:cmd-ver
   #:cmd-spoll-async
   #:cmd-wait))

(defpackage :tds
  (:use :cl :gpib :trivia)
  (:export
   #:tds-setup
   #:cmd
   #:ret
   #:query))
