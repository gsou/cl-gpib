(defsystem "cl-gpib"
  :description "GPIB communication using low cost Prologix style USB interfaces."
  :version "0.1"
  :depends-on ("cserial-port" "trivia")
  :author "gsou"
  :components
  ((:module "src"
    :pathname "src/"
    :serial t
    :components ((:file "packages")
                 (:file "interface")
                 (:file "tds")))))
